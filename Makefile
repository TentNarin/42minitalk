# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: nwattana <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2022/06/27 16:14:21 by nwattana          #+#    #+#              #
#    Updated: 2022/06/27 16:14:22 by nwattana         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

LIBFT_DIR = libftprintf/libft
LIBPRINTF_DIR = libftprintf

INC = -I$(LIBPRINTF_DIR)/includes\
       -I$(LIBFT_DIR)

LIBFT_PRINTF = $(LIBPRINTF_DIR)/libftprintf.a
UTILS = ./ft_minitalk_utils.c
SRC = main.c
OBJ = $(SRC:%.c=%.o)



CC = clang
CFLAG = -Wall -Wextra -Werror


all: minitalk

minitalk:lib
	$(CC) $(CFLAG) $(INC) $(LIBPRINTF_DIR)/libftprintf.a $(UTILS) server.c -o server
	$(CC) $(CFLAG) $(INC) $(LIBPRINTF_DIR)/libftprintf.a $(UTILS) client.c -o client

lib:
	make -C $(LIBPRINTF_DIR)

clean:
	make -C $(LIBPRINTF_DIR) clean

fclean: clean
	rm -rf server
	rm -rf client
	make -C $(LIBPRINTF_DIR) fclean
	
.PHONY: all test minitalk lib clean fclean 
