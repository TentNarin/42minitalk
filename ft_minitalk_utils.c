/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_utf_m.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nwattana <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/06/22 18:00:06 by nwattana          #+#    #+#             */
/*   Updated: 2022/06/27 16:19:05 by nwattana         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minitalk.h"

int	ft_ispid(char *str)
{
	while (*str)
	{
		if (!ft_isdigit(*str))
			return (0);
		str++;
	}
	return (1);
}

int	ft_sent_message(char *message, int pid)
{
	char	*temp;
	int		bit;

	temp = message;
	while (*temp)
	{
		bit = 128;
		while (bit > 0)
		{
			if (*temp & bit)
			{
				if (kill(pid, SIGUSR1) == -1)
					exit(0);
			}
			else
			{
				if (kill(pid, SIGUSR2) == -1)
					exit(0);
			}
			bit = bit >> 1;
			usleep(110);
		}
		temp++;
	}
	return (0);
}

void	received_sig(int sig)
{
	static char			value;
	static unsigned int	bit;

	if (sig == SIGUSR1)
	{
		value += powbit(8 - bit);
	}
	bit++;
	if (bit == 8)
	{
		write(1, &value, 1);
		value = 0;
		bit = 0;
	}
}

int	powbit(int a)
{
	int	res;

	res = 1;
	while (a > 1)
	{
		res = res * 2;
		a--;
	}
	return (res);
}
