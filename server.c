/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   server.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nwattana <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/06/23 22:14:57 by nwattana          #+#    #+#             */
/*   Updated: 2022/06/27 15:44:32 by nwattana         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minitalk.h"

void	ft_update_pid(int sig, int *c_pid);
void	received_sig_s(int sig);

int	main(void)
{
	struct sigaction	sa_usr;
	sigset_t			mask_set;

	sa_usr.sa_handler = &received_sig_s;
	sigemptyset(&mask_set);
	sigaddset(&mask_set, SIGUSR1);
	sigaddset(&mask_set, SIGUSR2);
	sa_usr.sa_mask = mask_set;
	sigaction(SIGUSR1, &sa_usr, NULL);
	sigaction(SIGUSR2, &sa_usr, NULL);
	ft_putnbr_fd(getpid(), 1);
	ft_putchar_fd('\n', 1);
	while (1)
	{
		pause();
	}
}

void	received_sig_s(int sig)
{
	static char	value;
	static int	bit;
	static int	c_pid;

	if (c_pid != 0)
	{
		if (sig == SIGUSR1)
			value += powbit(8 - bit);
		bit++;
		if (bit == 8)
		{
			if (value == 3)
			{
				kill(c_pid, SIGUSR1);
				c_pid = 0;
			}
			else
				write(1, &value, 1);
			bit = 0;
			value = 0;
		}
	}
	else
		ft_update_pid(sig, &c_pid);
}

void	ft_update_pid(int sig, int *c_pid)
{
	static int	temp;
	static int	bit;

	if (sig == SIGUSR1)
		temp += powbit(32 - bit);
	bit++;
	if (bit == 32)
	{
		*c_pid += temp;
		temp = 0;
		bit = 0;
	}
}
