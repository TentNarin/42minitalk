/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   minitalk.h                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nwattana <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/06/23 22:21:32 by nwattana          #+#    #+#             */
/*   Updated: 2022/09/26 04:15:37 by nwattana         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MINITALK_H
# define MINITALK_H

# include "libftprintf.h"
# include <signal.h>

int		powbit(int a);
int		ft_sent_message(char *message, int pid);
void	received_sig(int sig);
int		ft_ispid(char *str);
#endif
