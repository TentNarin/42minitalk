/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   client.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nwattana <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/06/25 10:04:27 by nwattana          #+#    #+#             */
/*   Updated: 2022/06/27 16:16:42 by nwattana         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minitalk.h"

void	ft_sent_three(int s_pid);
int		ft_sent_int(int num, int pid);
void	know(int sig);

int	main(int argc, char *argv[])
{
	int	my_pid;
	int	s_pid;

	if (argc != 3 || !ft_ispid(argv[1]) || !argv[2])
		return (0);
	signal(SIGUSR1, &know);
	s_pid = ft_atoi(argv[1]);
	my_pid = getpid();
	ft_sent_int(my_pid, s_pid);
	ft_sent_message(argv[2], s_pid);
	ft_sent_three(s_pid);
	while (1)
	{
		pause();
	}
	return (0);
}

void	ft_sent_three(int s_pid)
{
	kill(s_pid, SIGUSR2);
	usleep(100);
	kill(s_pid, SIGUSR2);
	usleep(100);
	kill(s_pid, SIGUSR2);
	usleep(100);
	kill(s_pid, SIGUSR2);
	usleep(100);
	kill(s_pid, SIGUSR2);
	usleep(100);
	kill(s_pid, SIGUSR2);
	usleep(100);
	kill(s_pid, SIGUSR1);
	usleep(100);
	kill(s_pid, SIGUSR1);
	usleep(100);
}

void	know(int sig)
{
	if (sig == SIGUSR1)
	{
		ft_putstr_fd("DONE\n", 1);
	}
	exit(0);
}

int	ft_sent_int(int num, int pid)
{
	unsigned int	bit;

	bit = -2147483648;
	while (bit > 0)
	{
		if (num & bit)
		{
			if (kill(pid, SIGUSR1) == -1)
			{
				exit(0);
			}
		}
		else
		{
			if (kill(pid, SIGUSR2) == -1)
			{
				exit(0);
			}
		}
		bit = bit >> 1;
		usleep(100);
	}
	return (0);
}
